# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'untitled.ui'
##
## Created by: Qt User Interface Compiler version 6.5.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QGridLayout, QGroupBox, QHBoxLayout,
    QMainWindow, QPushButton, QScrollArea, QSizePolicy,
    QSpacerItem, QTabWidget, QWidget)

from showimage import ShowImage

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1047, 706)
        MainWindow.setStyleSheet(u"QMainWindow {\n"
"	background-color:white;\n"
"}\n"
"\n"
"QWidget{\n"
"	background-color:white;\n"
"}\n"
"\n"
"QTextEdit {\n"
"	border-width: 1px; \n"
"	border-radius: 5px;\n"
"	border-color: rgb(0, 0, 0);\n"
"	border-style: solid;\n"
"	background-color:white;\n"
"	font-size: 14px;\n"
"	color: black;\n"
"}\n"
"\n"
"QLineEdit {\n"
"alignment:center;\n"
"	border-width: 1px; \n"
"	border-radius: 5px;\n"
"	border-color: rgb(0, 0, 0);\n"
"	border-style: inset;\n"
"	padding: 5px;\n"
"	color: black;\n"
"	background:white;\n"
"	selection-background-color:black;\n"
"	selection-color: white;\n"
"	font:14px;\n"
"}\n"
"\n"
"QLabel {\n"
"alignment:center;\n"
"	border-width: 0px; \n"
"	border-radius: 5px;\n"
"	border-color: rgb(0, 0, 0);\n"
"	border-style: inset;\n"
"	padding: 5px;\n"
"	color: rgb(0,0,0);\n"
"	background:white;\n"
"}\n"
"\n"
"QSpinBox {\n"
"	border-width: 2px; \n"
"	border-radius:5px;\n"
"	color: white;	\n"
"	background-color: black;\n"
"	padding: 5;\n"
"}\n"
"\n"
"QDoubleSpinBox {\n"
"	border-width: 2px; \n"
""
                        "	border-radius: 5px;\n"
"	color: white;	\n"
"	background-color: black;\n"
"	padding: 5;\n"
"}\n"
"\n"
"QProgressBar {\n"
"	text-align: center;\n"
"	color: rgb(0, 0, 0);\n"
"	border-width: 2px; \n"
"	border-radius: 10px;\n"
"	border-color: rgb(0, 0, 0);\n"
"	border-style: inset;\n"
"	background-color:white;\n"
"	padding:5px;\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"	background-color: gray;\n"
"	border-radius: 10px;\n"
"}\n"
"\n"
"QPushButton:hover\n"
"{\n"
"    color:#888888;\n"
"    background-color:rgb(0, 0, 0); \n"
"    border-style:inset;\n"
"    padding-left:8px;\n"
"    padding-top:8px;\n"
"}\n"
"\n"
"QPushButton:pressed\n"
"{\n"
"    color:#ffffff;\n"
"    background-color:rgb(0, 0, 0); \n"
"    border-style:inset;\n"
"    padding-left:6px;\n"
"    padding-top:6px;\n"
"}\n"
"\n"
"QPushButton\n"
"{\n"
"    color:black; \n"
"    background-color:rgb(255,255, 255);\n"
"    border-style:outset; \n"
"    border-width:2px;\n"
"    border-color:rgb(0, 0,0);\n"
"    border-radius:10px;\n"
"    font: 14px;\n"
"  "
                        "  min-width:100px;\n"
"    min-height:20px;\n"
"    padding:5px;\n"
"}\n"
"\n"
"QCheckBox {\n"
"	color: rgb(0,0,0);\n"
"	padding: 2px;\n"
"}\n"
"\n"
"QCheckBox:disabled {\n"
"	color: red;\n"
"	padding: 2px;\n"
"}\n"
"\n"
"QCheckBox::hover {\n"
"	border-radius:4px;\n"
"	border-style:solid;\n"
"	padding-left: 1px;\n"
"	padding-right: 1px;\n"
"	padding-bottom: 1px;\n"
"	padding-top: 1px;\n"
"	border-width:0px;    \n"
"	border-color: rgb(0, 0, 0);\n"
"	background-color:#000000;\n"
"	color: rgb(255,255,255);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"	height: 10px;\n"
"	width: 10px;\n"
"	border-style:solid;\n"
"	border-width: 1px;\n"
"	border-color: gray;\n"
"	background-color: gray;\n"
"}\n"
"QCheckBox::indicator:unchecked {\n"
"\n"
"	height: 10px;\n"
"	width: 10px;\n"
"	border-style:solid;\n"
"	border-width: 1px;\n"
"	border-color: gray;\n"
"	background-color: white;\n"
"}\n"
"\n"
"\n"
"QRadioButton {\n"
"	color: black;\n"
"	background-color: rgb(255,255,255);\n"
"	padding: 1px;\n"
"}\n"
"QRadioButton::ind"
                        "icator:checked {\n"
"	height: 10px;\n"
"	width: 10px;\n"
"	border-style:solid;\n"
"	border-radius:5px;\n"
"	border-width: 1px;\n"
"	border-color: gray;\n"
"	background-color: gray;\n"
"}\n"
"QRadioButton::indicator:!checked {\n"
"	height: 10px;\n"
"	width: 10px;\n"
"	border-style:solid;\n"
"	border-radius:5px;\n"
"	border-width: 1px;\n"
"	border-color: gray;\n"
"	background-color: transparent;\n"
"}\n"
"\n"
"QComboBox {\n"
"	color: black;	\n"
"	background: white;\n"
"	border-style:solid;\n"
"	border-width:2px;\n"
"	border-color:black;\n"
"	border-radius:5px;\n"
"	padding: 1px;\n"
"}\n"
"\n"
"QComboBox:editable {\n"
"	background: black;\n"
"	color: white;\n"
"}\n"
"\n"
"QComboBox:!editable {\n"
"	background: black;\n"
"	color: white;\n"
"}\n"
"\n"
"QComboBox QAbstractItemView {\n"
"	color: white;	\n"
"	background: black;\n"
"	selection-color: green;\n"
"	selection-background-color: gray;\n"
"}\n"
"\n"
"QTabWidget\n"
"{\n"
"}\n"
"\n"
"QTabWidget::pane {\n"
"		border-color: rgb(0,0,0);\n"
"		border-style: solid;\n"
""
                        "		border-width: 0px;\n"
"		border-radius: 6px;\n"
"}\n"
"\n"
"QTabBar::tab {	\n"
"	color:gray;\n"
"	font: 14pt ;\n"
"	min-width: 100px;\n"
"	min-height: 30px;\n"
"	padding: 5px;\n"
"	border-width: 1px;\n"
"	border-style: solid;\n"
"	border-bottom-color: black; \n"
"	border-top-left-radius: 10px;\n"
"	border-top-right-radius: 10px;\n"
"	margin-right:5px;\n"
"	margin-left:5px;\n"
" \n"
"}\n"
"\n"
"QTabWidget::tab-bar{\n"
"    alignment: center;\n"
"}\n"
"\n"
"QTabBar::tab:selected\n"
"{\n"
"    background-color: black;\n"
"    color:rgb(255,255,255);\n"
"    font: 14pt;\n"
"}\n"
"\n"
"QGroupBox {\n"
"    background-color: white;\n"
"    border: 1px solid black;\n"
"    border-radius: 5px;\n"
"    margin-top: 1px;\n"
" 	padding: 10px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"	color:gray;\n"
"    padding: 0px 0px 0px 0px;\n"
"	margin:0px 0px 0px 0px;\n"
"    background-color:white;\n"
"}\n"
"\n"
"QScrollArea {\n"
"  	 border-color: gray;\n"
"     border-style: solid;\n"
"	 border-width:0px;\n"
" 	 border-radius: 0px;"
                        "\n"
"}\n"
"\n"
"QScrollBar:vertical {\n"
"     border-color: gray;\n"
"     border-style: solid;\n"
"	 border-width:2px;\n"
" 	 border-radius: 0px;\n"
"	 color:red;\n"
"     background: white;\n"
" 	 margin: 25px 0px 25px 0px;\n"
"}\n"
"QScrollBar::handle:vertical {\n"
"	border-color: gray;\n"
"     border-style: solid;\n"
"	 border-width:2px;\n"
" 	 border-radius: 3px;\n"
"	margin: 1px 2px 1px 2px;\n"
"    background: gray;\n"
"    min-width: 20px;\n"
"}\n"
"\n"
"QScrollBar::add-line:vertical {\n"
"   border-color: gray;\n"
"    border-style: solid;\n"
"	border-width:1px;\n"
" 	border-radius: 0px;\n"
"    background: black;\n"
"    height: 20px;\n"
"    subcontrol-position: bottom;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:vertical {\n"
"   border-color: gray;\n"
"    border-style: solid;\n"
"	border-width:1px;\n"
" 	border-radius: 0px;\n"
"    background: black;\n"
"    height: 20px;\n"
"    subcontrol-position: top;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar:up"
                        "-arrow:vertical, QScrollBar::down-arrow:vertical {\n"
"	border: 2px solid gray;\n"
"    width: 5px;\n"
"    height: 5px;\n"
"    background: white;\n"
"}\n"
"\n"
"QScrollBar:horizontal {\n"
"    border-color: gray;\n"
"     border-style: solid;\n"
"	 border-width:2px;\n"
" 	 border-radius: 0px;\n"
"	 color:red;\n"
"     background: white;\n"
"    height: 20px;\n"
"    margin: 0px 25px 0px 25px;\n"
"}\n"
"QScrollBar::handle:horizontal {\n"
"   border-color: gray;\n"
"     border-style: solid;\n"
"	 border-width:2px;\n"
" 	 border-radius: 3px;\n"
"	margin: 1px 2px 1px 2px;\n"
"    background: gray;\n"
"    min-width: 20px;\n"
"}\n"
"QScrollBar::add-line:horizontal {\n"
"  border-color: gray;\n"
"    border-style: solid;\n"
"	border-width:1px;\n"
" 	border-radius: 0px;\n"
"    background: black;\n"
"    width: 20px;\n"
"    subcontrol-position: right;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:horizontal {\n"
"  border-color: gray;\n"
"    border-style: solid;\n"
"	border-width:1px;\n"
""
                        " 	border-radius: 0px;\n"
"    background: black;\n"
"    width: 20px;\n"
"    subcontrol-position: left;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar:left-arrow:horizontal, QScrollBar::right-arrow:horizontal {\n"
"   border: 2px solid gray;\n"
"    width: 5px;\n"
"    height: 5px;\n"
"    background: white;\n"
"}\n"
"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal {\n"
"    background: none;\n"
"}\n"
"\n"
"")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout = QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        self.gridLayout = QGridLayout(self.groupBox)
        self.gridLayout.setObjectName(u"gridLayout")
        self.widget_image = ShowImage(self.groupBox)
        self.widget_image.setObjectName(u"widget_image")

        self.gridLayout.addWidget(self.widget_image, 0, 0, 1, 1)


        self.horizontalLayout.addWidget(self.groupBox)

        self.groupBox_2 = QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.groupBox_2.setMinimumSize(QSize(300, 0))
        self.groupBox_2.setMaximumSize(QSize(300, 16777215))
        self.gridLayout_2 = QGridLayout(self.groupBox_2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.tabWidget = QTabWidget(self.groupBox_2)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.gridLayout_3 = QGridLayout(self.tab)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.scrollArea = QScrollArea(self.tab)
        self.scrollArea.setObjectName(u"scrollArea")
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 242, 587))
        self.gridLayout_4 = QGridLayout(self.scrollAreaWidgetContents)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.pbtn_load_image = QPushButton(self.scrollAreaWidgetContents)
        self.pbtn_load_image.setObjectName(u"pbtn_load_image")
        self.pbtn_load_image.setMinimumSize(QSize(114, 34))
        self.pbtn_load_image.setMaximumSize(QSize(16777215, 40))

        self.gridLayout_4.addWidget(self.pbtn_load_image, 0, 0, 1, 1)

        self.pbtn_fit_image = QPushButton(self.scrollAreaWidgetContents)
        self.pbtn_fit_image.setObjectName(u"pbtn_fit_image")
        self.pbtn_fit_image.setMinimumSize(QSize(114, 34))
        self.pbtn_fit_image.setMaximumSize(QSize(16777215, 40))

        self.gridLayout_4.addWidget(self.pbtn_fit_image, 1, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 526, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_4.addItem(self.verticalSpacer, 2, 0, 1, 1)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.gridLayout_3.addWidget(self.scrollArea, 0, 0, 1, 1)

        self.tabWidget.addTab(self.tab, "")

        self.gridLayout_2.addWidget(self.tabWidget, 0, 0, 1, 1)


        self.horizontalLayout.addWidget(self.groupBox_2)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.groupBox.setTitle(QCoreApplication.translate("MainWindow", u"\u56fe\u7247", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("MainWindow", u"\u64cd\u4f5c", None))
        self.pbtn_load_image.setText(QCoreApplication.translate("MainWindow", u"\u52a0\u8f7d\u4e00\u5f20\u56fe\u7247", None))
        self.pbtn_fit_image.setText(QCoreApplication.translate("MainWindow", u"\u56fe\u7247\u9002\u5e94\u7a97\u53e3", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("MainWindow", u"\u4e3b\u64cd\u4f5c", None))
    # retranslateUi

