'''
类名:ImageDealWith
说明:图片处理类
功能:
    1.加载图片
注意:
    文件中已经建立_image_deal_with=ImageDealWith() 可以直接调用模块使用
著作权信息:
    作者:照彩云归
    联系方式:
版本信息:
    日期:2023-12-25
    版本:v1.0.0
    描述:基本类
'''

from PySide6.QtCore import QObject
from PySide6.QtGui import QImage,QPixmap,QPainter,QImageReader

class ImageDealWith(QObject):
    '''图片'''
    CurrentImage=None

#初始化
    def __init__(self,parent=None):
        super(ImageDealWith,self).__init__(parent)
        self.CurrentImage=QImage()
        pass

    '''
    读取图片
    '''
    def LoadImage(self,image_path_):
        read_dir_=QImageReader(image_path_)
        self.CurrentImage=read_dir_.read()
        pass

_image_deal_with=ImageDealWith()
