'''
类名:ShowImage
说明:显示图片
功能:
    1.显示QImage图片
    2.可以拖拉，缩放图片
    3.提示图片坐标
    4.提示图片坐标点像素
注意:

著作权信息:
    作者:照彩云归
    联系方式:mai15816994584
版本信息:
    日期:2023-12-25
    版本:v1.0.0
    描述:基本类
'''

from PySide6.QtWidgets import (QApplication, QGridLayout, QGroupBox, QHBoxLayout,
    QMainWindow, QPushButton, QScrollArea, QSizePolicy,
    QSpacerItem, QTabWidget, QWidget,QLabel)
from PySide6.QtGui import QMouseEvent,QPainter,QFont,QPen,QColor,QBrush
from PySide6.QtCore import QPoint,QPointF,Qt
from PySide6 import QtCore
import PySide6

from imagedealwith import imagedealwith

class ShowImage(QWidget):

###初始化
    def __init__(self,parent=None):
        super(ShowImage,self).__init__(parent)
        self._scale = 1
        self._dy = 0
        self._dx = 0
        self._start_x = 0
        self._start_y = 0
        self._mouse_down = False
        self._board_width=self.width()
        self._board_height=self.height()
        self._font=QFont()
        self._font.setFamily("Microsoft YaHei")
        self._font.setPointSize(20)
        self._font.setBold(True)
        self._font.setLetterSpacing(QFont.SpacingType.AbsoluteSpacing,3)
        self._pen=QPen()
        self._pen.setBrush(QBrush(QColor(0,255,0)));
        self._pen.setWidth(10);
        self.setMouseTracking(True);
        self._labBoardCoor = QLabel(self);
        self._labBoardCoor.setGeometry(10, 0, 300, 30)
        self._labBoardCoor.setText("0,0")

###使用窗口
    def FitWindow(self):
        win_width_ = self.width();
        win_height_ = self.height();
        ###获取板的大小
        if imagedealwith._image_deal_with.CurrentImage!=None:
            if ((imagedealwith._image_deal_with.CurrentImage.width()>0)
                &(imagedealwith._image_deal_with.CurrentImage.height()>0)):
                self._board_width = imagedealwith._image_deal_with.CurrentImage.width();
                self._board_height = imagedealwith._image_deal_with.CurrentImage.height();
                pass
        else:
            self._board_height=self.width()
            self._board_height=self.height()

        ###获取窗口与画板比例
        width_scale_ = win_width_ / self._board_width;
        height_scale_ = win_height_ / self._board_height;
        ###计算缩放
        if width_scale_ < height_scale_:
             self._scale = width_scale_
        else:
            self._scale = height_scale_

        ###偏移为零
        self._dx = 0
        self._dy = 0

        ####计算适合的长宽
        fitWidth_ = self._board_width * self._scale;
        fitHeight_ = self._board_height * self._scale;

        ###计算开始坐标
        self._start_x = (win_width_ - fitWidth_) / 2;
        self._start_y = (win_height_ - fitHeight_) / 2;

        ###刷新控件
        self.update()

###刷新显示
    def RefleshShow(self):
        self.update()
###移动
    def SetMove(self,dx_,dy_):
        self._dx = dx_;
        self._dy = dy_;
        self.update();
        pass

###缩放
    def SetScale(self,scale_):
        self._scale*=scale_
        self.update()
        pass

###设置板的大小
    def SetBoardSize(self,width_,height_):
        self._board_width=width_
        self._board_height=height_
        pass

###获取点在板里面的位置
    def GetPointInBoard(self,point_:QPointF):
        po_=QPointF()
        po_.setX((point_.x() - self._start_x-self._dx) / self._scale);
        po_.setY((point_.y() - self._start_y-self._dy) / self._scale);
        return po_

###获取点在板里面的位置
    def GetPointInBoar(self,x_,y_):
        po_ = QPointF()
        po_.setX((x_ - self._start_x - self._dx) / self._scale);
        po_.setY((y_ - self._start_y - self._dy) / self._scale);
        return po_

###获取点在窗口里面的坐标
    def GetPointInWin(self,point_:QPointF):
        po_ = QPointF();
        po_.setX(point_.x() * self._scale + self._start_x + self._dx);
        po_.setY(point_.y() * self._scale + self._start_y +self._dy);
        return po_;

###滚轮
    def wheelEvent(self, event: PySide6.QtGui.QWheelEvent):
        super().wheelEvent(event)
        if ((imagedealwith._image_deal_with.CurrentImage.width() == 0)
                & (imagedealwith._image_deal_with.CurrentImage.height() == 0)):
            return

        ### 获取鼠标窗口坐标
        win_point1_ = event.position();
        ###鼠标点在画板中没有缩放的坐标
        original_x_ = (win_point1_.x() - self._start_x-self._dx) / self._scale;
        original_y_ = (win_point1_.y() - self._start_y-self._dy) / self._scale;

        if event.angleDelta().y() > 0:
            self._scale *= 1.2
        else:
            self._scale *= 0.8

        win_point2_=QPointF()
        ### 缩放后鼠标原来画板中的坐标对应窗口坐标
        win_point2_.setX(original_x_ * self._scale + self._start_x + self._dx);
        win_point2_.setY(original_y_ * self._scale + self._start_y + self._dy);
        ### 更新开始坐标
        self._start_x = self._start_x + ((win_point1_.x() - win_point2_.x()));
        self._start_y = self._start_y + ((win_point1_.y() - win_point2_.y()));

        self.update();

###鼠标按下
    def mousePressEvent(self, event: PySide6.QtGui.QMouseEvent):
        super(ShowImage, self).mousePressEvent(event)
        if ((imagedealwith._image_deal_with.CurrentImage.width() == 0)
                & (imagedealwith._image_deal_with.CurrentImage.height() == 0)):
            return

        ##btn_status_=PySide6.QtCore.M

        if event.button()==Qt.MouseButton.LeftButton:
            self._mouse_down = True
            self._down_point = event.pos()
            self._last_point = event.pos()

###鼠标释放
    def mouseReleaseEvent(self, event: PySide6.QtGui.QMouseEvent):
        super(ShowImage, self).mouseReleaseEvent(event)
        if ((imagedealwith._image_deal_with.CurrentImage.width() == 0)
                & (imagedealwith._image_deal_with.CurrentImage.height() == 0)):
            return

        if self._mouse_down == True:
            self._mouse_down = False;

###鼠标移动
    def mouseMoveEvent(self, event: PySide6.QtGui.QMouseEvent):
        super(ShowImage, self).mouseMoveEvent(event)
        if ((imagedealwith._image_deal_with.CurrentImage.width() == 0)
                & (imagedealwith._image_deal_with.CurrentImage.height() == 0)):
            return

        if self._mouse_down==True:
            self._last_point = event.pos()
            self._dx += self._last_point.x() - self._down_point.x()
            self._dy += self._last_point.y() - self._down_point.y()
            self._down_point = self._last_point
            self.update()
        else:
            point_ = event.pos()
            x_ = (point_.x() - self._start_x-self._dx) / self._scale
            y_ = (point_.y() - self._start_y-self._dy) / self._scale
            if ((x_ < 0 )| (x_ > self._board_width)
                    | (y_ < 0) | (y_ >self._board_height)):
                x_ = 0
                y_ = 0

            rgb_ = imagedealwith._image_deal_with.CurrentImage.pixelColor(x_, y_).getRgb()
            coor_=str(round(x_,2))+","+str(round(y_,2))+"-rgb:"+str(rgb_[0])+","+str(rgb_[1])+","+str(rgb_[2])
            self._labBoardCoor.setText(coor_)

###绘制事件
    def paintEvent(self, event: PySide6.QtGui.QPaintEvent):
        super().paintEvent(event)
        if ((imagedealwith._image_deal_with.CurrentImage.width() == 0)
                & (imagedealwith._image_deal_with.CurrentImage.height() == 0)):
            return

        painter_=QPainter(self)

        blackboard_ = event.rect();
        painter_.fillRect(blackboard_, QBrush(QColor(0,0,0)))

        moveX_ =self._start_x + self._dx;
        moveY_ = self._start_y + self._dy;

        ###根据参数创建坐标系
        painter_.translate(moveX_, moveY_);
        painter_.scale(self._scale,self._scale);
        painter_.drawImage(0, 0, imagedealwith._image_deal_with.CurrentImage)

        painter_.setFont(self._font);
        painter_.setPen(self._pen);

        painter_.end();
